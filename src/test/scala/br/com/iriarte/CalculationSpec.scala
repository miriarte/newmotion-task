package br.com.iriarte

import br.com.iriarte.model._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{ Matchers, WordSpec }

class CalculationSpec extends WordSpec with Matchers with ScalaFutures {

  "Calculation" should {
    "correctly calculate startFee" in {
      val s = Session("jose", "2020-01-28T00:00:00Z", "2020-01-28T01:00:00Z", 2)
      val t = Tariff("EUR", Some(10), None, None, "2019-01-28T00:00:00Z")
      val st = SessionWithTariff(s, t)

      Calculation(st).amount shouldBe s"EUR10.00"
    }
    "correctly calculate time based fee" in {
      val s = Session("jose", "2020-01-28T00:00:00Z", "2020-01-28T02:30:00Z", 2)
      val t = Tariff("EUR", None, Some(2.4), None, "2019-01-28T00:00:00Z")
      val st = SessionWithTariff(s, t)

      Calculation(st).amount shouldBe s"EUR6.00"
    }
    "correctly calculate power usage fee" in {
      val s = Session("jose", "2020-01-28T00:00:00Z", "2020-01-28T02:30:00Z", 4.5)
      val t = Tariff("EUR", None, None, Some(2.1), "2019-01-28T00:00:00Z")
      val st = SessionWithTariff(s, t)

      Calculation(st).amount shouldBe s"EUR9.45"
    }
    "correctly calculate All fess" in {
      val s = Session("jose", "2020-01-28T00:00:00Z", "2020-01-28T02:30:00Z", 5) //2 and half hours
      val t = Tariff("EUR", Some(2), Some(3), Some(4), "2019-01-28T00:00:00Z")
      val st = SessionWithTariff(s, t)

      Calculation(st).amount shouldBe s"EUR29.50"
    }
    "correctly calculate in a different currency" in {
      val s = Session("jose", "2020-01-28T00:00:00Z", "2020-01-28T02:30:00Z", 5) //2 and half hours
      val t = Tariff("JPY", Some(2), Some(3), Some(4), "2019-01-28T00:00:00Z")
      val st = SessionWithTariff(s, t)

      Calculation(st).amount shouldBe s"JPY29.50"
    }

    "correctly calculate using whole hour only" in {
      val s = Session("jose", "2020-01-28T00:00:00Z", "2020-01-28T02:30:00Z", 5) //2 hours (lost half)
      val t = Tariff("EUR", Some(2), Some(3), Some(4), "2019-01-28T00:00:00Z")
      val st = SessionWithTariff(s, t)

      Calculation(st)(WholeHour).amount shouldBe s"EUR28.00"
    }
  }
}
