package br.com.iriarte.model.spec

import br.com.iriarte.model.{ SessionDataAccess, TariffDataAccess }

case class ModelHelper(tda: Option[TariffDataAccess] = None, sda: Option[SessionDataAccess] = None) {

  def cleanTariffs() = tda.foreach(_.clean())
  def cleanSessions() = sda.foreach(_.clean())
}
