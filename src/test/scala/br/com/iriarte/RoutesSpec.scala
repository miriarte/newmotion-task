package br.com.iriarte
import akka.actor.{ ActorRef, Props }
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import br.com.iriarte.CsvSerializer.SessionWithCost
import br.com.iriarte.model._
import br.com.iriarte.model.spec.ModelHelper
import br.com.iriarte.spec.VTITariffActor
import io.circe.generic.auto._
import kantan.csv._
import kantan.csv.ops._
import kantan.csv.generic._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{ BeforeAndAfterEach, Matchers, WordSpec }

import scala.concurrent.Await
import scala.concurrent.duration._
class RoutesSpec extends WordSpec with Matchers with ScalaFutures with ScalatestRouteTest
  with Routes with BeforeAndAfterEach {

  lazy val tariffDA: TariffDataAccess = new TariffDataAccess
  lazy val sessionDA: SessionDataAccess = new SessionDataAccess
  lazy val modelHelper = ModelHelper(Some(tariffDA), Some(sessionDA))
  override val vtiTariffActor: ActorRef =
    system.actorOf(Props(new VTITariffActor(tariffDA)), "vtiTRegistry")

  override val vtiSessionActor: ActorRef =
    system.actorOf(Props(new VTISessionsActor(tariffDA, sessionDA)), "vtiSRegistry")

  lazy val routes: Route = allRoutes

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    modelHelper.cleanTariffs()
    modelHelper.cleanSessions()
  }

  "TariffRoutes (POST /tariff)" should {
    "be able to add a complete tariff " in {
      val dateKey = "2019-01-01T00:00:00.000Z"
      val t = Tariff("EUR", Some(0.2), Some(1), Some(0.25), dateKey)
      val entity = Marshal(t).to[MessageEntity].futureValue

      val request = Post("/tariff").withEntity(entity)

      request ~> routes ~> check {
        status should ===(StatusCodes.Created)
      }
      tariffDA.findExact(dateKey) shouldBe Some(t)
    }
    "be able to add a tariff with missing fees (some but not all) " in {
      val dateKey = "2020-01-01T00:00:00.000Z"
      val t = Tariff("EUR", Some(0.2), None, None, dateKey)
      val entity = Marshal(t).to[MessageEntity].futureValue

      val request = Post("/tariff").withEntity(entity)

      request ~> routes ~> check {
        status should ===(StatusCodes.Created)
      }
      tariffDA.findExact(dateKey) shouldBe Some(t)
    }

    "not be able to add a tariff with missing all fees  " in {
      val dateKey = "2020-01-01T00:00:00.000Z"
      val t = Tariff("EUR", None, None, None, dateKey)
      val entity = Marshal(t).to[MessageEntity].futureValue

      val request = Post("/tariff").withEntity(entity)

      request ~> routes ~> check {
        status should ===(StatusCodes.BadRequest)
        contentType should ===(ContentTypes.`application/json`)

        entityAs[BusinessError] shouldBe BusinessError("Invalid tariff: all fees are empty")
      }
      tariffDA.findExact(dateKey) shouldBe None
    }

    "not be able to add a tariff with wrong currency" in {
      val dateKey = "2020-01-01T00:00:00.000Z"
      val t = Tariff("???", Some(1), Some(1), Some(1), dateKey)
      val entity = Marshal(t).to[MessageEntity].futureValue

      val request = Post("/tariff").withEntity(entity)

      request ~> routes ~> check {
        status should ===(StatusCodes.BadRequest)
        contentType should ===(ContentTypes.`application/json`)

        entityAs[BusinessError].msg shouldBe s"Invalid tariff: currency code not valid"
      }
      tariffDA.findExact(dateKey) shouldBe None
    }

    "not be able to add a tariff in the past  " in {
      val dateKey = "2000-01-01T00:00:00.000Z"
      val t = Tariff("EUR", Some(1), Some(1), Some(1), dateKey)
      val entity = Marshal(t).to[MessageEntity].futureValue

      val request = Post("/tariff").withEntity(entity)

      request ~> routes ~> check {
        status should ===(StatusCodes.BadRequest)
        contentType should ===(ContentTypes.`application/json`)

        entityAs[BusinessError].msg shouldBe s"This datetime '$dateKey' should be latter than now"
      }
      tariffDA.findExact(dateKey) shouldBe None
    }

    "not be able to add a tariff if there is a most recent already  " in {
      val dateKey = "2020-01-01T00:00:00.000Z"
      val t = Tariff("EUR", Some(1), Some(1), Some(1), dateKey)
      val entity = Marshal(t).to[MessageEntity].futureValue

      val request = Post("/tariff").withEntity(entity)
      request ~> routes ~> check {
        status should ===(StatusCodes.Created)
      }
      tariffDA.findExact(dateKey) shouldBe Some(t)

      val dateKey2 = "2019-01-01T00:00:00.000Z"
      val t2 = Tariff("EUR", Some(1), Some(1), Some(1), dateKey2)
      val entity2 = Marshal(t2).to[MessageEntity].futureValue

      val request2 = Post("/tariff").withEntity(entity2)

      request2 ~> routes ~> check {
        status should ===(StatusCodes.BadRequest)
        contentType should ===(ContentTypes.`application/json`)
        entityAs[BusinessError].msg should
          startWith("This datetime '2019-01-01T00:00:00.000Z' should be latter than '2020")
      }
      tariffDA.findExact(dateKey2) shouldBe None
    }

    "not be able to add a tariff if the date is invalid" in {
      val dateKey = "INVALID_DATE"
      val t = Tariff("EUR", Some(1), Some(1), Some(1), dateKey)
      val entity = Marshal(t).to[MessageEntity].futureValue

      val request = Post("/tariff").withEntity(entity)

      request ~> routes ~> check {
        status should ===(StatusCodes.BadRequest)
        contentType should ===(ContentTypes.`application/json`)

        entityAs[BusinessError].msg shouldBe s"Date not parseable '$dateKey'"
      }
      tariffDA.findExact(dateKey) shouldBe None
    }
  }

  "SessionRoutes (POST /session)" should {

    def addTariff(tariff: Tariff) = {
      val request = Post("/tariff").withEntity(Marshal(tariff).to[MessageEntity].futureValue)

      request ~> routes ~> check {
        status should ===(StatusCodes.Created)
      }
    }
    val commonTariff = Tariff("EUR", Some(1), Some(1), Some(1), "2019-01-01T00:00:00.000Z")
    val postTariff = Tariff("USD", Some(0), Some(0), Some(1), "2019-01-02T00:00:00.000Z")
    val farTariff = Tariff("USD", Some(0), Some(0), Some(1), "2020-01-02T00:00:00.000Z")
    "be able to add a complete session " in {

      addTariff(commonTariff)

      val session = Session("Client1", "2019-01-02T00:00:00.000Z", "2019-01-03T00:00:00.000Z", 10)

      val entity = Marshal(session).to[MessageEntity].futureValue

      val request = Post("/session").withEntity(entity)

      request ~> routes ~> check {
        status should ===(StatusCodes.Created)
      }
      sessionDA.findExact(session.customerId, session.startTime).map(_.session) shouldBe Some(session)
    }
    "be able to add a complete session and a not overide the respective tariff" in {

      addTariff(commonTariff)
      val session = Session("Client1", "2019-01-04T00:00:00.000Z", "2019-01-05T00:00:00.000Z", 10)

      val entity = Marshal(session).to[MessageEntity].futureValue

      val request = Post("/session").withEntity(entity)

      request ~> routes ~> check {
        status should ===(StatusCodes.Created)
      }
      sessionDA.findExact(session.customerId, session.startTime).map(_.session) shouldBe Some(session)

      addTariff(postTariff)
      sessionDA.findExact(session.customerId, session.startTime).map(_.session) shouldBe Some(session)
    }

    // this can be tricky but I'm assuming that an user can charge 2 simultaneous equipments at the same time
    "be able to add multiple sessions for the same client but different values" in {

      addTariff(commonTariff)
      val session = Session("Client1", "2019-01-04T00:00:00.000Z", "2019-01-05T00:00:00.000Z", 10)

      val entity = Marshal(session).to[MessageEntity].futureValue

      val request = Post("/session").withEntity(entity)

      request ~> routes ~> check {
        status should ===(StatusCodes.Created)
      }
      sessionDA.findExact(session.customerId, session.startTime).map(_.session) shouldBe Some(session)

      val session2 = Session("Client1", "2019-01-04T00:00:00.000Z", "2019-01-10T00:00:00.000Z", 10)

      val entity2 = Marshal(session2).to[MessageEntity].futureValue

      val request2 = Post("/session").withEntity(entity2)

      request2 ~> routes ~> check {
        status should ===(StatusCodes.Created)
      }
      sessionDA.findExact(session2.customerId, session2.startTime).map(_.session) shouldBe Some(session2)

    }

    "not be able to add a session if there is no tariff interval" in {

      addTariff(farTariff)

      val session = Session("Client1", "2019-01-10T00:00:00.000Z", "2019-01-12T00:00:00.000Z", 10)

      val entity = Marshal(session).to[MessageEntity].futureValue

      val request = Post("/session").withEntity(entity)

      request ~> routes ~> check {
        status should ===(StatusCodes.BadRequest)
        contentType should ===(ContentTypes.`application/json`)

        entityAs[BusinessError].msg should startWith("No tariff assigned for this session in ")
      }
      sessionDA.findExact(session.customerId, session.startTime).map(_.session) shouldBe None
    }

    "not be able to add a session if startTime is malformed" in {

      addTariff(farTariff)

      val session = Session("Client1", "Invalid", "2019-01-12T00:00:00.000Z", 10)

      val entity = Marshal(session).to[MessageEntity].futureValue

      val request = Post("/session").withEntity(entity)

      request ~> routes ~> check {
        status should ===(StatusCodes.BadRequest)
        contentType should ===(ContentTypes.`application/json`)

        entityAs[BusinessError].msg should startWith("Invalid session: start time")
      }
      sessionDA.findExact(session.customerId, session.startTime).map(_.session) shouldBe None
    }

    "not be able to add a session if endTime is malformed" in {

      addTariff(farTariff)

      val session = Session("Client1", "2019-01-12T00:00:00.000Z", "Invalid", 10)

      val entity = Marshal(session).to[MessageEntity].futureValue

      val request = Post("/session").withEntity(entity)

      request ~> routes ~> check {
        status should ===(StatusCodes.BadRequest)
        contentType should ===(ContentTypes.`application/json`)

        entityAs[BusinessError].msg should startWith("Invalid session: end time")
      }
      sessionDA.findExact(session.customerId, session.startTime).map(_.session) shouldBe None
    }

    "not be able to add a session if both time are malformed" in {

      addTariff(farTariff)

      val session = Session("Client1", "-", "Invalid", 10)

      val entity = Marshal(session).to[MessageEntity].futureValue

      val request = Post("/session").withEntity(entity)

      request ~> routes ~> check {
        status should ===(StatusCodes.BadRequest)
        contentType should ===(ContentTypes.`application/json`)

        entityAs[BusinessError].msg should startWith("Invalid session: both dates")
      }
      sessionDA.findExact(session.customerId, session.startTime).map(_.session) shouldBe None
    }

    "not be able to add a session if volume is not a positive value" in {

      addTariff(farTariff)

      val session = Session("Client1", "2019-01-10T00:00:00.000Z", "2019-01-12T00:00:00.000Z", -0.5)

      val entity = Marshal(session).to[MessageEntity].futureValue

      val request = Post("/session").withEntity(entity)

      request ~> routes ~> check {
        status should ===(StatusCodes.BadRequest)
        contentType should ===(ContentTypes.`application/json`)

        entityAs[BusinessError].msg should startWith("Invalid session: you need to add a positive volume")
      }
      sessionDA.findExact(session.customerId, session.startTime).map(_.session) shouldBe None
    }

    "not be able to add a session if startTime is later than endTime" in {

      addTariff(commonTariff)

      val session = Session("Client1", "2019-01-10T00:00:00.000Z", "2019-01-02T00:00:00.000Z", 14)

      val entity = Marshal(session).to[MessageEntity].futureValue

      val request = Post("/session").withEntity(entity)

      request ~> routes ~> check {
        status should ===(StatusCodes.BadRequest)
        contentType should ===(ContentTypes.`application/json`)

        entityAs[BusinessError].msg should startWith("Invalid session: endDate '")
      }
      sessionDA.findExact(session.customerId, session.startTime).map(_.session) shouldBe None
    }

  }

  "SessionRoutes (GET /sessions)" should {

    val firstSession = Session("me", "2020-01-28T00:00:00Z", "2020-01-28T01:00:00Z", 2)
    def addSession(session: Session) = {
      val request = Post("/session").withEntity(Marshal(session).to[MessageEntity].futureValue)

      request ~> routes ~> check {
        status should ===(StatusCodes.Created)
      }
    }

    def addTariff(tariff: Tariff) = {
      val request = Post("/tariff").withEntity(Marshal(tariff).to[MessageEntity].futureValue)

      request ~> routes ~> check {
        status should ===(StatusCodes.Created)
      }
    }

    val commonTariff = Tariff("EUR", Some(1), Some(1), Some(1), "2019-01-01T00:00:00.000Z")
    val postTariff = Tariff("USD", Some(0), Some(0), Some(1), "2019-01-02T00:00:00.000Z")
    val farTariff = Tariff("USD", Some(0), Some(0), Some(1), "2020-01-02T00:00:00.000Z")
    "download an empty json" in {

      val request = Get("/sessions.json")

      request ~> routes ~> check {
        status should ===(StatusCodes.OK)

        val l = entityAs[List[SessionWithCost]]

        l shouldBe empty
      }
    }

    "download a full json" in {
      addTariff(commonTariff)
      addSession(firstSession)
      val request = Get("/sessions.json")

      request ~> routes ~> check {
        status should ===(StatusCodes.OK)
        val l = entityAs[List[SessionWithCost]]

        l shouldNot be(empty)
      }
    }

    "download an empty csv" in {

      val request = Get("/sessions.csv")

      request ~> routes ~> check {
        status should ===(StatusCodes.OK)
        val timeout = 300.millis
        val body = Await.result(responseEntity.toStrict(timeout).map { _.data }.map(_.utf8String), timeout)
        val l = body.asCsvReader[SessionWithCost](rfc.withHeader)

        l shouldBe empty
      }
    }

    "download a full csv" in {
      addTariff(commonTariff)
      addSession(firstSession)
      val request = Get("/sessions.csv")

      request ~> routes ~> check {
        status should ===(StatusCodes.OK)
        val timeout = 300.millis
        val body = Await.result(responseEntity.toStrict(timeout).map { _.data }.map(_.utf8String), timeout)
        val l = body.asCsvReader[SessionWithCost](rfc.withHeader)

        l shouldNot be(empty)
      }
    }

    "fail on non existent serializer" in {

      val request = Get("/sessions.lalala")

      request ~> routes ~> check {
        status should ===(StatusCodes.BadRequest)
      }
    }
  }
}
