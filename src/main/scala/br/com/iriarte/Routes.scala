package br.com.iriarte

import akka.actor.{ ActorRef, ActorSystem }
import akka.event.Logging
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.StatusCodes.{ BadRequest, Created, InternalServerError }
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import akka.pattern.ask
import akka.stream.scaladsl.FileIO
import akka.util.Timeout
import br.com.iriarte.VTISessionsActor.{ DownloadSessions, SetSession }
import br.com.iriarte.model._
import br.com.iriarte.spec.VTITariffActor.SetTariff
import io.circe.generic.auto._

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.io.{ Source => ScalaSource }
import scala.util.{ Failure, Success }

trait Routes extends JsonSupport {
  implicit def system: ActorSystem
  lazy val log = Logging(system, classOf[Routes])
  def vtiTariffActor: ActorRef
  def vtiSessionActor: ActorRef
  // Required by the `ask` (?) method below
  implicit lazy val timeout: Timeout = Timeout(5.seconds) // usually we'd obtain the timeout from the system's configuration

  lazy val tariffsRoute: Route =
    pathPrefix("tariff") {
      concat(
        pathEnd {
          concat(
            post {
              entity(as[Tariff]) { tariff =>

                val result: Future[Either[BusinessError, Boolean]] =
                  (vtiTariffActor ? SetTariff(tariff)).mapTo[Either[BusinessError, Boolean]]
                onComplete(result) {
                  case Success(r) =>
                    r match {
                      case Left(v)  => complete(ToResponseMarshallable(BadRequest -> v))
                      case Right(_) => complete(HttpResponse(Created))
                    }

                  case Failure(f) => complete(ToResponseMarshallable(InternalServerError -> f))
                }
              }
            }
          )
        }
      )
    }
  lazy val sessionRoute: Route = pathPrefix("session") {
    concat(
      pathEnd {
        concat(
          post {
            entity(as[Session]) { s =>
              val result: Future[Either[BusinessError, Boolean]] =
                (vtiSessionActor ? SetSession(s)).mapTo[Either[BusinessError, Boolean]]
              onComplete(result) {
                case Success(r) =>
                  r match {
                    case Left(v)  => complete(ToResponseMarshallable(BadRequest -> v))
                    case Right(_) => complete(HttpResponse(Created))
                  }
                case Failure(f) => complete(ToResponseMarshallable(InternalServerError -> f))
              }
            }
          }
        )
      }
    )
  } ~ path("sessions." ~ RemainingPath.?) { ext =>
    concat(
      get {
        val serializer: SeqSerializers = ext match {
          case Some(s) if s.toString().toUpperCase == "JSON" => JsonSerializer
          case Some(s) if s.toString().toUpperCase == "CSV" => CsvSerializer
          case _ => NoSerializer
        }

        if (serializer.isValid) {
          val result: Future[Seq[SessionWithTariff]] =
            (vtiSessionActor ? DownloadSessions).mapTo[Seq[SessionWithTariff]]
          onComplete(result) {
            case Success(r) =>
              val (contentType, f) = serializer.serialize(r)
              complete(HttpEntity(contentType, FileIO.fromPath(f().toPath)))
            case Failure(f) => complete(ToResponseMarshallable(InternalServerError -> f))
          }
        } else {
          complete(ToResponseMarshallable(BadRequest -> BusinessError("Serialization model not found")))
        }
      }
    )
  }

  lazy val allRoutes = handleRejections(myRejectionHandler) { tariffsRoute ~ sessionRoute }

  implicit def myRejectionHandler: RejectionHandler =
    RejectionHandler.newBuilder()
      .handle {
        case MalformedRequestContentRejection(e, _) =>
          complete(ToResponseMarshallable(BadRequest -> BusinessError("Request rejected, probably malformed json. See api.json for correct schema")))
      }
      .result()

}
