package br.com.iriarte

import akka.actor.{ Actor, ActorLogging, Props }
import br.com.iriarte.VTISessionsActor.{ DownloadSessions, SetSession }
import br.com.iriarte.model._

object VTISessionsActor {
  final case class SetSession(session: Session)
  final case object DownloadSessions
  def props: Props = Props[VTISessionsActor]
}

class VTISessionsActor(tariffDA: TariffDataAccess, sessionDA: SessionDataAccess) extends Actor with ActorLogging {

  private def checkValidFormat(s: Session): SessionDBResponse = {
    (s.startTimeZDT, s.endTimeZDT, s.volume) match {
      case (Left(_), Left(_), _) => Left(BusinessError(s"Invalid session: both dates (startTime='${s.startTime}', endTime='${s.endTime}') are invalid"))
      case (Left(_), _, _)       => Left(BusinessError(s"Invalid session: start time '${s.startTime}' is invalid"))
      case (_, Left(_), _)       => Left(BusinessError(s"Invalid session: end time '${s.endTime}' is invalid"))
      case (_, _, v) if v < 0    => Left(BusinessError(s"Invalid session: you need to add a positive volume"))
      case _                     => Right(s)
    }
  }

  private def checkValidDates(s: Session): SessionDBResponse = {
    val check = for {
      start <- s.startTimeZDT
      end <- s.endTimeZDT
    } yield start.isBefore(end)

    check.flatMap {
      case false => Left(BusinessError(s"Invalid session: endDate '${s.endTime}' should be after startDate '${s.startTime}'"))
      case _     => Right(s)
    }

  }

  private def checkHasPaymentInterval(s: Session): SessionWithTariffDBResponse = {
    // if this falls here, I guarantee that `startTimeZDT.toOption` is defined
    val tariff = tariffDA.findAdequate(s.startTimeZDT.toOption.get)
    tariff match {
      case Some(t) => Right(SessionWithTariff(s, t))
      case None    => Left(BusinessError(s"No tariff assigned for this session in startDate=${s.startTime}"))
    }
  }

  def receive: Receive = {
    case SetSession(session) =>
      // I'm assuming that my map will not break at all, just return true if the activeStarting could be parsed and
      // we could upsert this row or the BusinessError
      val response = Right(session)
        .flatMap(checkValidFormat)
        .flatMap(checkValidDates)
        .flatMap(checkHasPaymentInterval)
        .flatMap(sessionDA.addSessionWithTariff)
      sender() ! response
    case DownloadSessions => sender() ! sessionDA.dumpAll()
  }
}