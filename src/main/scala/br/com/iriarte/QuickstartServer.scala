package br.com.iriarte

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.{ ExceptionHandler, Route }
import akka.stream.ActorMaterializer
import br.com.iriarte.model.{ SessionDataAccess, TariffDataAccess }
import br.com.iriarte.spec.VTITariffActor

object QuickstartServer extends App with Routes {

  implicit val system: ActorSystem = ActorSystem("AkkaHttpServer")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  lazy val tariffDA = new TariffDataAccess
  lazy val sessionDA = new SessionDataAccess

  val vtiTariffActor: ActorRef = system.actorOf(Props(new VTITariffActor(tariffDA)), "vtiTariffActor")
  val vtiSessionActor: ActorRef = system.actorOf(Props(new VTISessionsActor(tariffDA, sessionDA)), "vtiSessionActor")

  lazy val routes: Route = allRoutes
  //#http-server
  Http().bindAndHandle(routes, "localhost", 8080)

  println(s"Server online at http://localhost:8080/")

  Await.result(system.whenTerminated, Duration.Inf)
}
