package br.com.iriarte.model

import java.time.{ Instant, ZoneOffset, ZonedDateTime }

import br.com.iriarte.utils.DateTimeUtils._

import scala.collection.{ Map, SortedMap }
import scala.concurrent.stm._

class TariffDataAccess {
  implicit lazy val zdtOrdering: Ordering[ZonedDateTime] = (x: ZonedDateTime, y: ZonedDateTime) => x compareTo y
  implicit lazy val zdtTariffOrdering: Ordering[(ZonedDateTime, Option[Tariff])] =
    (x: (ZonedDateTime, Option[Tariff]), y: (ZonedDateTime, Option[Tariff])) => x._1 compareTo y._1
  private val sentinel: ZonedDateTime = Instant.ofEpochMilli(Long.MaxValue).atZone(ZoneOffset.UTC)
  private val defaultMap = SortedMap[ZonedDateTime, Option[Tariff]](sentinel -> None)
  private lazy val tariffMap = Ref(defaultMap)

  def add(t: Tariff): AddDBResponse = {
    t.index.flatMap {
      i =>
        {
          atomic { implicit txn =>
            tariffMap() = tariffMap() + (i -> Option(t))
            Right(true)
          }
        }
    }
  }

  def findExact(dateString: String): Option[Tariff] = atomic { implicit txn =>
    val searchValue = strToUTCZonedDateTime(dateString)
    val s: SortedMap[ZonedDateTime, Option[Tariff]] = tariffMap()
    searchValue.toOption.flatMap(s.get).flatten
  }

  def findMax(): Option[(ZonedDateTime, Option[Tariff])] = atomic { implicit txn =>
    tariffMap().filterNot(_._1 == sentinel) match {
      case s if s.isEmpty => None
      case s              => Some(s.maxBy(_._1))
    }
  }

  //used only to extract one or more values from a Map
  object MapExtractor {
    def unapplySeq[A, B](s: Map[A, B])(implicit ev1: A => A, ev2: B => B): Option[Seq[(A, B)]] =
      Some(s.toSeq)
  }

  def findAdequate(d: ZonedDateTime): Option[Tariff] =
    atomic { implicit txn =>
      val s: SortedMap[ZonedDateTime, Option[Tariff]] = tariffMap()

      // if it is valid, we shouldn't search over a empty Map
      if (s.nonEmpty) {
        // here we will try to get two adjacent datetimes to build a datetime interval
        s.sliding(2).find {
          // if there is only one value
          case MapExtractor(x)        => x._1.isBefore(d)
          // if there is two or more values(in fact ignoring the rest)
          case MapExtractor(x, y, _*) => x._1.isBefore(d) && y._1.isAfter(d)
        }.flatMap(_.head._2)
      } else {
        None
      }

    }

  private[model] def clean(): Unit = {
    atomic { implicit txn =>
      tariffMap() = defaultMap
    }
  }
}
