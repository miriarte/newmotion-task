package br.com.iriarte

import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import java.util.Currency

import br.com.iriarte.utils.DateTimeUtils.strToUTCZonedDateTime

package object model {
  type TariffDBResponse = Either[BusinessError, Tariff]

  case class BusinessError(msg: String)

  // I'll not parse activeStarting for now as I'll use as key a UTC-ed version of this field
  case class Tariff(
      currency: String,
      startFee: Option[Double],
      hourlyFee: Option[Double],
      feePerKWh: Option[Double],
      activeStarting: String) {
    // I'm moving every datetime to UTC, makes it simpler to do search operations
    def index: Either[BusinessError, ZonedDateTime] = strToUTCZonedDateTime(activeStarting)

    def isIndexable: Boolean = index.isRight
  }

  type SessionDBResponse = Either[BusinessError, Session]
  type SessionWithTariffDBResponse = Either[BusinessError, SessionWithTariff]

  case class Session(
      customerId: String,
      startTime: String,
      endTime: String,
      volume: Double
  ) {
    def startTimeZDT: Either[BusinessError, ZonedDateTime] = strToUTCZonedDateTime(startTime)

    def endTimeZDT: Either[BusinessError, ZonedDateTime] = strToUTCZonedDateTime(endTime)

    def isValidSession: Boolean = startTimeZDT.isRight && endTimeZDT.isRight && volume >= 0
  }

  case class SessionWithTariff(session: Session, tariff: Tariff)

  type AddDBResponse = Either[BusinessError, Boolean]

  object Calculation {
    def apply(sessionWithTariff: SessionWithTariff)(implicit strictness: Strictness = HourByMinute): Calculation = {
      val startCost = sessionWithTariff.tariff.startFee.getOrElse(0d)
      val timeCost = (for {
        e <- sessionWithTariff.session.endTimeZDT.toOption
        s <- sessionWithTariff.session.startTimeZDT.toOption
        f <- sessionWithTariff.tariff.hourlyFee
      } yield strictness.between(e, s) * f).getOrElse(0d)
      val usageCost = sessionWithTariff.tariff.feePerKWh.map(_ * sessionWithTariff.session.volume).getOrElse(0d)
      val total: Double = startCost + timeCost + usageCost
      val formatter = java.text.NumberFormat.getCurrencyInstance
      formatter.setCurrency(Currency.getInstance(sessionWithTariff.tariff.currency))

      new Calculation(s"${formatter.format(total)}")
    }
  }

  class Calculation(val amount: String)(implicit strictness: Strictness = HourByMinute)

  trait Strictness {
    def between(s: ZonedDateTime, e: ZonedDateTime): Double
  }

  case object WholeHour extends Strictness {
    override def between(s: ZonedDateTime, e: ZonedDateTime): Double = ChronoUnit.HOURS.between(e, s).toDouble
  }

  case object HourByMinute extends Strictness {
    override def between(s: ZonedDateTime, e: ZonedDateTime): Double = ChronoUnit.MINUTES.between(e, s).toDouble / 60
  }

}