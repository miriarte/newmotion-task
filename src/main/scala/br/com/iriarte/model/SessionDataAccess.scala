package br.com.iriarte.model

import scala.concurrent.stm._

class SessionDataAccess {

  private lazy val sessionSet = TSet.empty[SessionWithTariff]

  def addSessionWithTariff(st: SessionWithTariff): AddDBResponse = atomic { implicit txn =>
    Right(sessionSet.add(st))
  }

  def findExact(customerId: String, startTime: String) = atomic { implicit txn =>
    sessionSet.find(s => s.session.customerId == customerId && s.session.startTime == startTime)
  }

  def dumpAll(): Seq[SessionWithTariff] = sessionSet.single.toSeq

  private[model] def clean(): Unit = {
    atomic { implicit txn =>
      sessionSet.clear()
    }
  }
}
