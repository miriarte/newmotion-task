package br.com.iriarte

import java.io.{ File, PrintWriter }

import akka.http.scaladsl.model.{ ContentType, ContentTypes }
import br.com.iriarte.model.{ Calculation, SessionWithTariff }
import io.circe.generic.auto._
import io.circe.syntax._
import kantan.csv._
import kantan.csv.ops._
import kantan.csv.generic._

trait SeqSerializers {
  def isValid: Boolean
  def serialize(seq: Seq[(SessionWithTariff)]): (ContentType, () => File)
}

case object NoSerializer extends SeqSerializers {
  override def isValid: Boolean = false

  override def serialize(seq: Seq[(SessionWithTariff)]): (ContentType.WithCharset, () => File) =
    (ContentTypes.`text/plain(UTF-8)`, () => File.createTempFile("fake", "fake"))

}

case object JsonSerializer extends SeqSerializers {

  case class SessionWithCost(
      customerId: String,
      startTime: String,
      endTime: String,
      volume: Double,
      costFromThisSession: String)
  override def isValid: Boolean = true

  override def serialize(seq: Seq[(SessionWithTariff)]): (ContentType.WithFixedCharset, () => File) = {
    val result = seq.map(s => {
      val sess = s.session
      SessionWithCost(sess.customerId, sess.startTime, sess.endTime, sess.volume, Calculation(s).amount)
    })
    val out = java.io.File.createTempFile("sessions.json", "json")
    val writer = new PrintWriter(out)
    writer.write(result.asJson.noSpaces)
    writer.close()
    (ContentTypes.`application/json`, () => out)
  }
}

case object CsvSerializer extends SeqSerializers {

  case class SessionWithCost(
      customerId: String,
      startTime: String,
      endTime: String,
      volume: Double,
      costFromThisSession: String)
  override def isValid: Boolean = true

  override def serialize(seq: Seq[(SessionWithTariff)]): (ContentType.WithCharset, () => File) = {
    val result = seq.map(s => {
      val sess = s.session
      SessionWithCost(sess.customerId, sess.startTime, sess.endTime, sess.volume, Calculation(s).amount)
    })

    val out = java.io.File.createTempFile("sessions.csv", "csv")
    val writer =
      out.asCsvWriter[SessionWithCost](rfc.withHeader("CustomerId", "StartTime", "EndTime", "Volume", "Debt"))
    writer.write(result)
    writer.close()
    (ContentTypes.`text/csv(UTF-8)`, () => out)
  }
}

