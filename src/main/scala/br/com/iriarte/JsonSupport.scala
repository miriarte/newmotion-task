package br.com.iriarte

import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport

trait JsonSupport extends FailFastCirceSupport
