package br.com.iriarte.utils

import java.time.{ ZoneOffset, ZonedDateTime }

import br.com.iriarte.model.BusinessError

import scala.util.Try

object DateTimeUtils {

  def strToUTCZonedDateTime(strDate: String): Either[BusinessError, ZonedDateTime] =
    Try(ZonedDateTime.parse(strDate).withZoneSameInstant(ZoneOffset.UTC))
      .toEither.left.map(v => BusinessError(s"Date not parseable '$strDate'"))
}
