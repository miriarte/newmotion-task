package br.com.iriarte.spec

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.Currency

import akka.actor.{ Actor, ActorLogging, Props }
import br.com.iriarte.model.{ BusinessError, Tariff, TariffDBResponse, TariffDataAccess }

import scala.util.{ Failure, Success, Try }
object VTITariffActor {
  final case class SetTariff(tariff: Tariff)
  def props: Props = Props[VTITariffActor]
}

class VTITariffActor(tariffDA: TariffDataAccess) extends Actor with ActorLogging {
  import VTITariffActor._

  private def checkValidFee(t: Tariff): TariffDBResponse = {
    (t.feePerKWh, t.hourlyFee, t.startFee) match {
      case (None, None, None) => Left(BusinessError(s"Invalid tariff: all fees are empty"))
      case _                  => Right(t)
    }
  }

  private def checkValidCurrency(t: Tariff): TariffDBResponse = {
    Try(Currency.getInstance(t.currency)) match {
      case Failure(_) => Left(BusinessError(s"Invalid tariff: currency code not valid"))
      case Success(_) => Right(t)
    }
  }

  private def checkValidDate(t: Tariff): TariffDBResponse = {
    val maxTime = tariffDA.findMax().map(_._1)
    (t.index, maxTime) match {
      case (Left(v), _) => Left(v)
      case (Right(i: ZonedDateTime), Some(m)) if !i.isAfter(m) =>
        Left(BusinessError(s"This datetime '${t.activeStarting}' should be latter than '${m.format(DateTimeFormatter.ISO_ZONED_DATE_TIME)}'"))
      case (Right(i: ZonedDateTime), _) if !i.isAfter(ZonedDateTime.now()) =>
        Left(BusinessError(s"This datetime '${t.activeStarting}' should be latter than now"))
      case _ => Right(t)
    }
  }

  def receive: Receive = {
    case SetTariff(tariff) =>
      // I'm assuming that my map will not break at all, just return true if the activeStarting could be parsed and
      // we could upsert this row or the BusinessError
      val response = Right(tariff)
        .flatMap(checkValidFee)
        .flatMap(checkValidCurrency)
        .flatMap(checkValidDate)
        .flatMap(tariffDA.add)
      sender() ! response
  }
}