import scalariform.formatter.preferences._

lazy val akkaHttpVersion = "10.0.11"
lazy val akkaVersion    = "2.5.11"
lazy val circeVersion = "0.9.3"
resolvers += Resolver.bintrayRepo("hseeberger", "maven")
lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization    := "br.com.iriarte",
      scalaVersion    := "2.12.4"
    )),
    name := "VenerableInertia Back-office",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"            % akkaHttpVersion,
      "de.heikoseeberger" %% "akka-http-circe"      % "1.20.1",
      "org.scala-stm"     %% "scala-stm"            % "0.8",
      "com.nrinaudo"      %% "kantan.csv"           % "0.4.0",
      "com.nrinaudo"      %% "kantan.csv-java8"     % "0.4.0",
      "com.nrinaudo"      %% "kantan.csv-generic"   % "0.4.0",
      "com.nrinaudo"      %% "kantan.csv-cats"      % "0.4.0",
      "io.circe"          %% "circe-core"           % circeVersion,
      "io.circe"          %% "circe-generic"        % circeVersion,
      "io.circe"          %% "circe-parser"         % circeVersion,

      "com.typesafe.akka" %% "akka-http-testkit"    % akkaHttpVersion % Test,
      "com.typesafe.akka" %% "akka-testkit"         % akkaVersion     % Test,
      "org.scalatest"     %% "scalatest"            % "3.0.1"         % Test
    )
  )


scalariformPreferences := scalariformPreferences.value
  .setPreference(AlignSingleLineCaseStatements, true)
  .setPreference(DoubleIndentConstructorArguments, true)
  .setPreference(DanglingCloseParenthesis, Preserve)

coverageEnabled := true

coverageExcludedPackages := "<empty>;RoutesSpec.scala;ModelHelper.scala;package.scala;QuickStartServer.scala"
