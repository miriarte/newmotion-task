### The New Motion assignment
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/a7a311d0d1384d019e59206b4ac53223)](https://www.codacy.com?utm_source=git@bitbucket.org&amp;utm_medium=referral&amp;utm_content=miriarte/newmotion-task&amp;utm_campaign=Badge_Grade)

All assignment information is described in [newmotion-assignment.md](./newmotion-assignment.md). My current tech coice is:
- akka http 10.1
- circe 0.9.3 _for json serialization_
- kantan.csv 0.4.0 _for csv serialization_
- scala-stm 0.8 _for an alternative, but more reasonable, concurrency model on data layer_

The project was based on [https://github.com/akka/akka-http-quickstart-scala.g8] but akka-stream was removed as this assignment doesn't have a good use for it.

Some comments will be exposed below but more will be present on the code.

---

***Comments***:

- I've implemented some more restrictions that was specified on assigment. Doesn't make sense a session going back on time (endDate < startDate) =/
- I created tests for the most problematic parts of this system, given enough time I would build a more extensive test suite.
- My choice of concurrency model on database was STM because it makes more sense. Actor model is not suited to transactional environment, so implementing this layer using this would be an error, although easier.
- Instead of doing csv serialization by hand, I've relied on kantan-csv.

---

***Miscellaneous files and how-to run***

Inside `misc` folder there are 2 files:
- [api.json](./misc/api.json) => is an OpenApi Specification file (aka Swagger) so you could use that to build your client code or just to make sure that the server is following this contract
- [VTI.postman_collection.json](./misc/VTI.postman_collection.json) => is a Postman collection if you want to use =)

***How to run:***

- Inside the project root folder, run :`./sbt run` It will run on [http://localhost:8080](http://localhost:8080)

_wow, really that easy?_ Yes, but as any scala project it will download half of the internet. So please, be patient.

- If you want to run the tests as well: `./sbt test`
- To check the test coverage: `./sbt clean coverage test coverageReport`




